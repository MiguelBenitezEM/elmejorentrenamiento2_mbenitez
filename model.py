def roman_natural(r_num):
    # Funcion para convertir numeros romanos a naturales
    n_num = 0
    dict_roman = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}
    for i in range(len(r_num)):
        if i == 0 or dict_roman[r_num[i]] <= dict_roman[r_num[i-1]]:
            n_num += dict_roman[r_num[i]]
        else:
            n_num +=  dict_roman[r_num[i]] - 2 * dict_roman[r_num[i-1]]
    return n_num