import sys
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QApplication, QGridLayout, QLabel, QLineEdit, QPushButton
from model import roman_natural


class Task(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        grid = QGridLayout()
        grid.setSpacing(7)
        label = QLabel("Input", self)
        self.label_natural = QLabel()
        self.edit_roman = QLineEdit()
        button = QPushButton("Translate")

        button.clicked.connect(self.show_natural)

        grid.addWidget(label, 1, 0)
        grid.addWidget(self.edit_roman, 1, 1)
        grid.addWidget(button, 2, 1)
        grid.addWidget(self.label_natural, 3, 1)


        self.setLayout(grid)
        self.show()

    def show_natural(self):
        value = str(roman_natural(self.edit_roman.text()))
        self.label_natural.setText(value)


def main():
    app = QApplication(sys.argv)
    ex = Task()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()